#[cfg(feature = "tracing")]
use std::env;

#[cfg(feature = "tracing")]
use std::path::PathBuf;

#[cfg(feature = "tracing")]
use lttng_ust_generate::{Provider, Generator, CTFType, CIntegerType};


#[cfg(feature = "tracing")]
fn setup_tracepoints() {
    // the provider is called calendar
    let mut provider = Provider::new("calendar");

    // class for drawing events
    let draw_class = provider.create_class("draw")
        .add_field("width", CTFType::Integer(CIntegerType::I32))
        .add_field("height", CTFType::Integer(CIntegerType::I32));

    // drawing events
    draw_class.instantiate("draw_entry");
    draw_class.instantiate("draw_exit");
    draw_class.instantiate("draw_calendar_entry");
    draw_class.instantiate("draw_calendar_exit");
    draw_class.instantiate("draw_events_entry");
    draw_class.instantiate("draw_events_exit");

    // class for application events
    let app_class = provider.create_class("app");

    // application events
    app_class.instantiate("view_entry");
    app_class.instantiate("view_exit");

    Generator::default()
        .generated_lib_name("calendar_tracepoints")
        .register_provider(provider)
        .output_file_name(PathBuf::from(env::var("OUT_DIR").unwrap()).join("tracepoints.rs"))
        .generate()
        .expect("Unable to generate tracepoint bindings");
}

fn main() {
    #[cfg(feature = "tracing")]
    setup_tracepoints();
}
