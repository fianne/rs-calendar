#![windows_subsystem = "windows"]
//! Simple calendar applications.
mod app;
mod ui;
mod model;

use app::CalendarApp;
use iced::Task;
use std::{env, path};
use clap::Parser;
use log::info;
use simplelog::{SimpleLogger, Config};
use dirs;
use walkdir::WalkDir;

#[cfg(feature = "tracing")]
extern crate lttng_ust;

#[cfg(feature = "tracing")]
use lttng_ust::import_tracepoints;

#[cfg(feature = "tracing")]
import_tracepoints!(
    concat!(env!("OUT_DIR"), "/tracepoints.rs"),
    tracepoints
);

#[derive(Parser, Debug)]
#[command(version, about)]
struct CliArgs {
    files: Vec<String>,
}

fn is_calendar_file(entry: &path::PathBuf) -> bool {
	let file_name = entry.to_str().unwrap_or("");
	file_name.ends_with(".ics")
}

fn calendar_files() -> Vec<path::PathBuf> {
	if let Some(data_dir) = dirs::data_dir() {
		let calendar_dir = data_dir.join("calendars");
    	info!("calendar dir: {}", calendar_dir.to_str().unwrap_or("<none>"));
		WalkDir::new(calendar_dir)
			.into_iter()
			.filter_map(|e| e.ok())
			.map(|e| e.into_path())
			.filter(is_calendar_file)
			.collect()
	} else {
		Vec::new()
	}
}

pub fn main() -> iced::Result {
	set_up_logger();

    let args = CliArgs::parse();
    let calendar_files: Vec<_> = if args.files.is_empty() {
		calendar_files()
	} else {
		args.files.iter().map(|arg| {
	    	info!("CalFile: {}", arg);
	    	path::PathBuf::from(arg)
	    }).collect()
    };

	let app_factory = move || {
		(CalendarApp::with_files(calendar_files.clone()), Task::none())
		};
	iced::application("Calendar", CalendarApp::update, CalendarApp::view).run_with(app_factory)
}

fn set_up_logger() {
	let level = match env::var("RUST_LOG") {
		Err(_) => log::LevelFilter::Info,
		Ok(value) => match value.as_str() {
			"error" => log::LevelFilter::Error,
			"warn" => log::LevelFilter::Warn,
			"info" => log::LevelFilter::Info,
			"debug" => log::LevelFilter::Debug,
			"trace" => log::LevelFilter::Trace,
			"off" => log::LevelFilter::Off,
			_ => log::LevelFilter::Info // anything else maps to info
		}
	};
	SimpleLogger::init(level, Config::default()).unwrap();
}
