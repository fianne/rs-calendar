// For now, to implement a custom native widget you will need to add
// `iced_native` and `iced_wgpu` to your dependencies.
//
// Then, you simply need to define your widget type and implement the
// `iced_native::Widget` trait with the `iced_wgpu::Renderer`.
//
// Of course, you can choose to make the implementation renderer-agnostic,
// if you wish to, by creating your own `Renderer` trait, which could be
// implemented by `iced_wgpu` and other renderers.

use super::basics::CellGrid;
use crate::model::row::{CalendarRow, RowDay, CalendarPage};
use crate::model::events::EventType;
use crate::model::events::{Event, EventsCollection};
use chrono::{
	Datelike,
	Duration,
	Local,
	Locale,
	Months,
	NaiveDate,
	Weekday
};
use iced::advanced::text::Paragraph as _; // this is necessary to have Paragraph in scope and use its methods
use iced::{
	advanced::{
		text::{self, LineHeight, Shaping, Text},
		widget::{Tree, Widget},
		layout,
		renderer
	},
	mouse,
	alignment,
	Border,
	Color,
	Element,
	Length,
	Pixels,
	Point,
	Rectangle,
	Size,
	Theme
};

#[cfg(feature = "tracing")]
extern crate lttng_ust;

#[cfg(feature = "tracing")]
use lttng_ust::import_tracepoints;

#[cfg(feature = "tracing")]
import_tracepoints!(concat!(env!("OUT_DIR"), "/tracepoints.rs"), tracepoints);

// 5 weeks plus two extra days is enough to accomodate the longest months of 31 days
const YEAR_VIEW_DAYS_PER_ROW: u32 = 5 * 7 + 2;

//-------------------------------------------------------------------------

#[derive(Clone)]
pub struct CalendarParams {
    show_sidebar: bool,
    header_fg: Color,
    header_bg: Color,
    day_fg: Color,
    day_other_month_fg: Color,
    day_weekend_bg: Color,
    day_today_bg: Color,
    day_text_margin: f32,

    ev_height: f32,
    ev_margin: f32,
    ev_bg: Color,
    ev_bg_alt: Color,
    ev_fontsize: f32,
    pub ev_show_organizer: bool,
}

impl CalendarParams {
    pub fn new() -> Self {
        Self {
            show_sidebar: true,
            header_fg: Color::BLACK,
            header_bg: Color::TRANSPARENT,
            day_today_bg: Color::from_rgb8(214, 242, 252),
            day_fg: Color::BLACK,
            day_other_month_fg: Color::from_rgb8(220, 220, 220),
            day_weekend_bg: Color::from_rgb8(245, 245, 245),
            day_text_margin: 5.0,
            ev_height: 18.0,
            ev_margin: 2.0,
            ev_bg: Color::from_rgb8(200, 245, 200),
            ev_bg_alt: Color::from_rgb8(215, 215, 215), //Color::from_rgb8(200, 200, 250),
            ev_fontsize: 14.0,
            ev_show_organizer: true,
        }
    }

    pub fn bg_for_day(self: &Self, day: NaiveDate) -> Color {
        let weekday = day.weekday().num_days_from_monday();
        if day == Local::now().date_naive() {
            self.day_today_bg
        } else if weekday > 4 {
            self.day_weekend_bg
        } else {
            Color::TRANSPARENT
        }
    }
}

//-------------------------------------------------------------------------

fn render_events_in_row<Renderer>(
    params: &CalendarParams,
    renderer: &mut Renderer,
    cal_row: &CalendarRow,
    row_bounds: Rectangle,
    min_row_height: f32,
    font_size: Pixels,
    fg: Color,
    content: &str,
    events: &EventsCollection,
) where
    Renderer: text::Renderer,
{
    if cal_row.begin >= cal_row.end {
        return;
    }

    #[cfg(feature = "tracing")]
    tracepoints::calendar::draw_events_entry(row_bounds.width as i32, row_bounds.height as i32);

    #[derive(Debug)]
    struct EventBar<'a> {
        ev: &'a Event,
        bounds: Rectangle,
        bg: Color,
    }
    let paragraph = Renderer::Paragraph::with_text(Text {
        content,
        bounds: row_bounds.size(),
        size: font_size,
        line_height: LineHeight::default(),
        font: renderer.default_font(),
        horizontal_alignment: alignment::Horizontal::Left,
        vertical_alignment: alignment::Vertical::Top,
        shaping: Shaping::default(),
        wrapping: text::Wrapping::None,
    });
    let day_text_height = paragraph.min_height();

    // render events, if enough space
    let last_day = cal_row.end.pred_opt().unwrap();
    let all_events = events.within(cal_row.begin, last_day);
    let x = row_bounds.x;
    let y = row_bounds.y + params.day_text_margin + day_text_height;
    let ev_height = params.ev_height;
    let ev_margin = params.ev_margin;

    let mut ev_bars: Vec<EventBar> = all_events
        .iter()
        .filter(|e| e.ev_type == EventType::Regular)
        .map(|e| EventBar {
            ev: e,
            bounds: Rectangle {
                x,
                y,
                width: 0.0,
                height: ev_height,
            },
            bg: params.ev_bg,
        })
        .collect();

    // TODO: incompatible types num_days, grid num_cols
    let row_grid = CellGrid::new(
        row_bounds.x,
        row_bounds.y,
        row_bounds.width,
        row_bounds.height,
        cal_row.num_days().try_into().unwrap(),
        1,
    );

    let mut current_day = cal_row.begin;

	let mut ev_bar_stack: std::vec::Vec<usize> = std::vec::Vec::new();
	let no_event_index: usize = usize::MAX;

    // update event bars
    for cell in row_grid.iter() {
        for (ev_index, ev_bar) in ev_bars.iter_mut().enumerate() {
            if ev_bar.ev.begin == current_day || (ev_bar.ev.begin < cal_row.begin && current_day == cal_row.begin) {
                // start of event -> find first free slot in stack and set event bar position
                let pos = ev_bar_stack
                    .iter()
                    .position(|&index| index == no_event_index)
                    .unwrap_or_else(|| { ev_bar_stack.push(no_event_index); ev_bar_stack.len() - 1 });
                ev_bar_stack[pos] = ev_index;
                ev_bar.bounds.x = cell.x;
                ev_bar.bounds.y = y + (ev_height + ev_margin) * (pos as f32);
            }
            if ev_bar.ev.end == current_day {
                // end of event -> free slot in stack and set event bar width
                ev_bar.bounds.width = cell.x - ev_bar.bounds.x;
                let curr_pos = ev_bar_stack.iter().position(|&index| index == ev_index);
                let pos = curr_pos.unwrap();
                ev_bar_stack[pos] = no_event_index;
            }
        }
        current_day = current_day.succ_opt().unwrap();
    }

    for ev_bar in &mut ev_bars {
        // close events that exceed the row
        if ev_bar.ev.end >= current_day {
            ev_bar.bounds.width = row_bounds.x + row_bounds.width - ev_bar.bounds.x;
        }
        if row_bounds.y + min_row_height > ev_bar.bounds.y + ev_bar.bounds.height {
            renderer.fill_quad(
                renderer::Quad {
                    bounds: ev_bar.bounds,
                    border: Border {
                    	radius: 0.0.into(),
                    	width: 1.0,
                    	color: params.day_other_month_fg,
                    },
                    ..renderer::Quad::default()
                },
                ev_bar.bg,
            );
            let ev_text_size = Size {
				height: ev_bar.bounds.height - 2.0,
				width: ev_bar.bounds.width,
			};
            let ev_text_position = Point {
				x: ev_bar.bounds.x + 1.0,
				y: ev_bar.bounds.y,
			};

			let organizer_name = if params.ev_show_organizer {
				if let Some(organizer) = &ev_bar.ev.organizer {
					organizer.common_name.clone().unwrap_or(String::from("")) + ":"
				} else {
					String::from("")
				}
			} else {
				String::from("")
				};

			let event_label = organizer_name + ev_bar.ev.text.as_str();
            renderer.fill_text(
                Text {
                    content: event_label.into(),
                    bounds: ev_text_size,
                    size: params.ev_fontsize.into(),
                    line_height: LineHeight::default(),
                    font: renderer.default_font(),
                    horizontal_alignment: alignment::Horizontal::Left,
                    vertical_alignment: alignment::Vertical::Top,
                    shaping: Shaping::default(),
        			wrapping: text::Wrapping::None,
                },
                ev_text_position,
                fg,
                ev_bar.bounds,
            );
        }
    }
    #[cfg(feature = "tracing")]
    tracepoints::calendar::draw_events_exit(row_bounds.width as i32, row_bounds.height as i32);
}


//-------------------------------------------------------------------------

fn compute_month_name_width<Renderer>(renderer: &Renderer, bounds: Size, margin: f32, font_size: Pixels, locale: Locale) -> f32
where
    Renderer: text::Renderer,
{
    let mut max_month_width = 0.0;
    for month in 1..12 {
		let d = NaiveDate::from_ymd_opt(0, month, 1).unwrap();
        let paragraph = Renderer::Paragraph::with_text(Text {
            content: d.format_localized("%b", locale).to_string().as_str(),
            bounds,
            size: font_size,
            line_height: LineHeight::default(),
            font: renderer.default_font(),
            horizontal_alignment: alignment::Horizontal::Left,
            vertical_alignment: alignment::Vertical::Top,
            shaping: Shaping::default(),
        	wrapping: text::Wrapping::None,
        });
        let month_width = paragraph.min_width();
        if month_width > max_month_width {
            max_month_width = month_width;
        }
    }
    max_month_width + margin
}

//-------------------------------------------------------------------------

fn compute_week_num_width<Renderer>(renderer: &Renderer, bounds: Size, margin: f32, font_size: Pixels) -> f32
where
    Renderer: text::Renderer,
{
    let mut max_month_width = 0.0;
    let paragraph = Renderer::Paragraph::with_text(Text {
        content: "55",
        bounds,
        size: font_size,
        line_height: LineHeight::default(),
        font: renderer.default_font(),
        horizontal_alignment: alignment::Horizontal::Left,
        vertical_alignment: alignment::Vertical::Top,
        shaping: Shaping::default(),
        wrapping: text::Wrapping::None,
    });
    let month_width = paragraph.min_width();
    if month_width > max_month_width {
        max_month_width = month_width;
    }
    max_month_width + margin
}

//-------------------------------------------------------------------------

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum CalendarViewMode {
    Week,
    Month,
    Year,
}

pub struct CalendarView<'a> {
    first_day: NaiveDate,
    params: CalendarParams,
    mode: CalendarViewMode,
    events: &'a EventsCollection,
    row_name_font_size: f32,
    margin: f32,
    locale: Locale,
	calendar_page: CalendarPage,
}

impl<'a> CalendarView<'a> {
    pub fn new(mode: CalendarViewMode, params: &CalendarParams, day: NaiveDate, events: &'a EventsCollection) -> Self {
    	let first_day = match mode {
    		CalendarViewMode::Week => day.week(chrono::Weekday::Mon).first_day(),
    		CalendarViewMode::Month => day.with_day0(0).unwrap(),
    		CalendarViewMode::Year => day.with_month0(0).unwrap().with_day0(0).unwrap()
		};
        let calendar_page = match mode {
			CalendarViewMode::Week => CalendarPage::new_week_wide(1, first_day),
			CalendarViewMode::Month => CalendarPage::new_week_wide(6, first_day),
			CalendarViewMode::Year => CalendarPage::new_month_wide(12, first_day),
		};
    	CalendarView {
	    	first_day,
	    	params: params.clone(),
	    	mode,
	    	events,
	        row_name_font_size: 24.0,
	        margin: 10.0,
	        locale: Locale::it_IT,
	        calendar_page,
    	}
    }

    fn get_days_per_row(&self) -> u32 {
        match self.mode {
            CalendarViewMode::Week => 7, // one week -> 7 days
            CalendarViewMode::Month => 7, // one week per row -> 7 days
            CalendarViewMode::Year => YEAR_VIEW_DAYS_PER_ROW, // one month per row, aligned by weekday
        }
    }

    fn get_row_count(&self) -> u32 {
        match self.mode {
            CalendarViewMode::Week => 1, // just one week
            CalendarViewMode::Month => 6, // one week per row -> max 6 (incomplete) in a month
            CalendarViewMode::Year => 12, // one month per row
        }
    }

	fn get_calendar_row(&self, day: NaiveDate, row: u32) -> CalendarRow {
        match self.mode {
            CalendarViewMode::Week => CalendarRow::for_week(day + Duration::try_weeks(row.into()).unwrap()),
            CalendarViewMode::Month => CalendarRow::for_week(day + Duration::try_weeks(row.into()).unwrap()),
            CalendarViewMode::Year => CalendarRow::for_month(day + Months::new(row)),
        }
	}

	fn get_row_label(&self, cal_row: CalendarRow) -> String {
        match self.mode {
            CalendarViewMode::Week => (cal_row.begin.iso_week().week()).to_string(),
            CalendarViewMode::Month => (cal_row.begin.iso_week().week()).to_string(),
            CalendarViewMode::Year => cal_row.begin.format_localized("%b", self.locale).to_string(),
        }
	}

	fn get_sidebar_width(&self, renderer: &mut impl text::Renderer, bounds: Size) -> f32 {
        let sidebar_width = match self.mode {
            CalendarViewMode::Week => compute_week_num_width(renderer, bounds, self.margin, self.row_name_font_size.into()),
            CalendarViewMode::Month => compute_week_num_width(renderer, bounds, self.margin, self.row_name_font_size.into()),
            CalendarViewMode::Year => compute_month_name_width(renderer, bounds, self.margin, self.row_name_font_size.into(), self.locale),
        };
            // side column only visible if there is enough space
	    if self.params.show_sidebar && bounds.width > sidebar_width {
	        sidebar_width
	    } else {
	        0.0
	    }
	}

    fn draw_header(&self, renderer: &mut impl text::Renderer, bounds: Rectangle, side_w: f32) {
        // paint background over full width
        if self.params.header_bg != Color::TRANSPARENT {
            renderer.fill_quad(
                renderer::Quad {
                    bounds,
                    border: Border {
                    	radius: 0.0.into(),
                    	width: 0.0,
                    	color: Color::TRANSPARENT,
                    },
                    ..renderer::Quad::default()
                },
                self.params.header_bg,
            );
        }

        // redefine bounds to skip the side column
        let bounds = Rectangle {
            x: bounds.x + side_w,
            y: bounds.y,
            width: bounds.width - side_w,
            height: bounds.height,
        };

        // font dimension
        let font_size = renderer.default_size();

        let mut weekday = Weekday::Mon;

        let grid = CellGrid::new(
            bounds.x,
            bounds.y,
            bounds.width,
            bounds.height,
            self.get_days_per_row(),
            1,
        );
        for cell in grid.iter() {
            let bounds = Rectangle {
                x: cell.x + 0.5,
                y: cell.y + 0.5,
                width: cell.width,
                height: cell.height,
            };

            // background color of the day cell
            let bg_color = if weekday.number_from_monday() > 5 {
                self.params.day_weekend_bg
            } else {
                Color::TRANSPARENT
            };

            renderer.fill_quad(
                renderer::Quad {
                    bounds,
                    border: Border {
                    	radius: 0.0.into(),
                    	width: 0.0,
                    	color: Color::TRANSPARENT,
                    },
                    ..renderer::Quad::default()
                },
                bg_color,
            );

			let d = NaiveDate::from_isoywd_opt(2000, 1, weekday).unwrap();
			let mut day_initial = d.format_localized("%a", self.locale).to_string().to_uppercase();
			day_initial.truncate(1);

			weekday = weekday.succ();

            // color of text
            let fg = self.params.header_fg;

            let x = bounds.x + self.params.day_text_margin;
            let y = bounds.center_y();
            renderer.fill_text(
                Text {
                    content: day_initial,
                    bounds: bounds.size(),
                    size: font_size,
                    line_height: LineHeight::default(),
                    font: renderer.default_font(),
                    horizontal_alignment: alignment::Horizontal::Left,
                    vertical_alignment: alignment::Vertical::Center,
                    shaping: Shaping::default(),
        			wrapping: text::Wrapping::None,
                },
                Point { x, y },
                fg,
                bounds,
            );
        }
    }

    fn draw_sidebar(&self, renderer: &mut impl text::Renderer, bounds: Rectangle) {
        // dimensions of each box representing a row name
        let h: f32 = bounds.height / (self.get_row_count() as f32);

        for row in 0..self.get_row_count() {
        	let cal_row = self.get_calendar_row(self.first_day, row);
            // where to place the row name
            let row_name_bounds = Rectangle {
                x: bounds.x,
                y: (row as f32) * h + bounds.y + self.params.day_text_margin,
                width: bounds.width,
                height: h,
            };

            // render row name
            renderer.fill_text(
                Text {
                    content: self.get_row_label(cal_row),
                    bounds: row_name_bounds.size(),
                    size: self.row_name_font_size.into(),
                    line_height: LineHeight::default(),
                    font: renderer.default_font(),
                    horizontal_alignment: alignment::Horizontal::Left,
                    vertical_alignment: alignment::Vertical::Center,
                    shaping: Shaping::default(),
        			wrapping: text::Wrapping::None,
                },
                Point {
                    x: row_name_bounds.x + self.margin,
                    y: row_name_bounds.center_y(),
                },
                self.params.day_fg,
                row_name_bounds,
            );
        }
    }

    fn draw_days(&self, renderer: &mut impl text::Renderer, bounds: Rectangle) {
        // font dimension
        let font_size = renderer.default_size();

        let grid = CellGrid::new(
            bounds.x,
            bounds.y,
            bounds.width,
            bounds.height,
            self.get_days_per_row(),
            self.get_row_count(),
        );

        // use the minimum row height to compute available space for event bars
        // to avoid inconsistentencies when rows have slightly different heights
        // and some can fit more event bars than others
        let min_row_height = grid.compute_min_height();

		let mut row_index = 0;
		let row_num: f32 = self.calendar_page.get_row_count() as f32;
		let col_num = self.calendar_page.get_col_count() as i64;
		for row in self.calendar_page.get_rows() {

			let beg_y = (bounds.y + bounds.height * (row_index as f32) / row_num).trunc();
			let end_y = (bounds.y + bounds.height * ((row_index + 1) as f32) / row_num).trunc();

            let mut row_bounds: Rectangle = Rectangle {
                x: bounds.x,
                y: beg_y,
                width: bounds.width,
                height: end_y - beg_y,
            };
			let row_len: f32 = col_num as f32;
			for column_index in 0..col_num {

				let beg_x = (bounds.x + bounds.width * (column_index as f32) / row_len).trunc();
				let end_x = (bounds.x + bounds.width * ((column_index + 1) as f32) / row_len).trunc();

				let day_bounds = Rectangle{
					x: beg_x,
					y: beg_y,
					width: end_x - beg_x + 1.0,
					height: end_y - beg_y + 1.0,
				};
                let date_for_col = row.date_for_col(column_index);
				// draw the cell here
                if let RowDay::InRange(current_day) = date_for_col {

                    // update bounds for the rectangle with the actual days
                    if current_day == row.begin {
                        let diff = beg_x - row_bounds.x;
                        row_bounds.x = beg_x;
                        row_bounds.width -= diff;
                    } else if current_day + Duration::try_days(1).unwrap() == row.end {
                        row_bounds.width = end_x - row_bounds.x;
                    }

                    // label: day number
                    let t = current_day.day().to_string();
                    let content = t.as_str();

                    // color of text
                    let fg = self.params.day_fg;

                    // background color of the day cell
                    let bg_color = if self.events.is_any_in_day(current_day, &EventType::Background).is_some() {
						self.params.ev_bg_alt
					} else {
						self.params.bg_for_day(current_day)
					};

                    renderer.fill_quad(
                        renderer::Quad {
                            bounds: day_bounds,
		                    border: Border {
		                    	radius: 0.0.into(),
		                    	width: 1.0,
		                    	color: self.params.day_other_month_fg,
		                    },
		                    ..renderer::Quad::default()
                        },
                        bg_color,
                    );

                    // render day cell text
                    renderer.fill_text(
                        Text {
                            content: content.to_string(),
                            bounds: day_bounds.size(),
                            size: font_size,
                            line_height: LineHeight::default(),
                            font: renderer.default_font(),
                            horizontal_alignment: alignment::Horizontal::Left,
                            vertical_alignment: alignment::Vertical::Top,
                            shaping: Shaping::default(),
					        wrapping: text::Wrapping::None,
                        },
                        day_bounds.position(),
                        fg,
                        day_bounds,
                    );
                }
			}
            let content = "10";
            render_events_in_row(
                &self.params,
                renderer,
                row,
                row_bounds,
                min_row_height,
                font_size,
                self.params.day_fg,
                content,
                self.events,
            );

			row_index += 1;
		}
    }
}

impl<Message, Renderer> Widget<Message, Theme, Renderer> for CalendarView<'_>
where
    Renderer: text::Renderer,
{
    fn size(&self) -> Size<Length> {
        Size { width: Length::Fill, height: Length::Fill }
    }

    fn layout(&self, _tree: &mut Tree, _renderer: &Renderer, limits: &layout::Limits) -> layout::Node {
        layout::Node::new(limits.max())
    }

    fn draw(&self,
        _state: &Tree,
        renderer: &mut Renderer,
        _theme: &Theme,
        _style: &renderer::Style,
        layout: layout::Layout<'_>,
        _cursor: mouse::Cursor,
        _viewport: &Rectangle,
    ) {
        let bounds = layout.bounds();
        let margin: f32 = 20.0;

	    #[cfg(feature = "tracing")]
	    tracepoints::calendar::draw_calendar_entry(bounds.width as i32, bounds.height as i32);

        // side column only visible if there is enough space
        let sidebar_width = self.get_sidebar_width(renderer, bounds.size());

        // font and header dimension
        let font_size = f32::from(renderer.default_size());
        let first_row_h = font_size + margin;

        // header
        self.draw_header(
            renderer,
            Rectangle {
                height: first_row_h,
                ..bounds
            },
            sidebar_width,
        );

        // week column
        if sidebar_width > 0.0 {
            let x = bounds.x;
            let y = bounds.y + first_row_h;
            let width = sidebar_width;
            let height = bounds.height - first_row_h;
            self.draw_sidebar(
                renderer,
                Rectangle {
                    x,
                    y,
                    width,
                    height,
                },
            );
        }

        // monthly calendar cells
        let x = bounds.x + sidebar_width;
        let y = bounds.y + first_row_h;
        let width = bounds.width - sidebar_width;
        let height = bounds.height - first_row_h;
        self.draw_days(
            renderer,
            Rectangle {
                x,
                y,
                width,
                height,
            },
        );
    #[cfg(feature = "tracing")]
    tracepoints::calendar::draw_calendar_exit(bounds.width as i32, bounds.height as i32);
    }
}

impl<'a, Message, Renderer> From<CalendarView<'a>> for Element<'a, Message, Theme, Renderer>
where
    Renderer: text::Renderer,
{
    fn from(month_view: CalendarView<'a>) -> Self {
        Self::new(month_view)
    }
}
