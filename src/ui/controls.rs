use crate::{
	app::Message,
	ui::calendar::CalendarViewMode as ViewMode
};
use chrono::{
	Datelike,
	NaiveDate,
	Locale,
};
use iced::{
	Alignment,
	Background,
	Element,
	Length,
	Theme,
	alignment,
	theme::palette,
	border,
	widget::{button::{Status, Style, secondary}, row, text, button}
};

//#[derive(Default)]
pub struct Controls {
    pub mode: Option<ViewMode>,
    locale: Locale,
}

impl Default for Controls {
    fn default() -> Controls {
        Controls {
            mode : Some(ViewMode::Year),
            locale: Locale::it_IT,
        }
    }
}

impl Controls {
    pub fn view<'a>(&'a self, view_date: NaiveDate) -> Element<Message> {
        let description = match self.mode {
            Some(ViewMode::Month) => view_date.format_localized("%B", self.locale).to_string(),
            Some(ViewMode::Week) => view_date.format_localized("%B", self.locale).to_string(),
            _ => "".to_string()
        };
        row![
			button(text("=")).on_press(Message::ToggleSidebar).style(secondary),
        	row![
				button(text("W")).on_press(Message::ViewModeSelected(ViewMode::Week)).style(self.get_mode_style(ViewMode::Week)),
				button(text("M")).on_press(Message::ViewModeSelected(ViewMode::Month)).style(self.get_mode_style(ViewMode::Month)),
				button(text("Y")).on_press(Message::ViewModeSelected(ViewMode::Year)).style(self.get_mode_style(ViewMode::Year)),
			],
			row![
	                button(text("<")).on_press(self.get_msg_prev()).style(secondary),
	                button(text(">")).on_press(self.get_msg_next()).style(secondary),
			].spacing(0).padding(0),
            text(description)
                .width(Length::Fill)
                .align_x(alignment::Horizontal::Left)
                .size(24),
            text(view_date.year().to_string())
                .width(Length::Fill)
                .align_x(alignment::Horizontal::Right)
                .size(40),
            ]
            .align_y(Alignment::Center)
            .padding(5)
            .spacing(10)
            .into()
    }

    fn get_msg_next(&self) -> Message {
        match self.mode {
            Some(ViewMode::Week) => Message::NextWeek,
            Some(ViewMode::Month) => Message::NextMonth,
            Some(ViewMode::Year) => Message::NextYear,
            None => todo!(),
        }
    }

    fn get_msg_prev(&self) -> Message {
        match self.mode {
            Some(ViewMode::Week) => Message::PrevWeek,
            Some(ViewMode::Month) => Message::PrevMonth,
            Some(ViewMode::Year) => Message::PrevYear,
            None => todo!(),
        }
    }

    fn get_mode_style(&self, view_mode: ViewMode) -> impl Fn(&Theme, Status) -> Style {
		if let Some(current_view_mode) = self.mode {
			if current_view_mode == view_mode {
				mode_selected
			} else {
				mode_not_selected
			}
		} else {
			todo!()
		}
	}
}

// TODO: copied from iced button
fn styled(pair: palette::Pair) -> Style {
    Style {
        background: Some(Background::Color(pair.color)),
        text_color: pair.text,
        border: border::rounded(2),
        ..Style::default()
    }
}
fn disabled(style: Style) -> Style {
    Style {
        background: style
            .background
            .map(|background| background.scale_alpha(0.5)),
        text_color: style.text_color.scale_alpha(0.5),
        ..style
    }
}

fn mode_not_selected(theme: &Theme, status: Status) -> Style {
    let palette = theme.extended_palette();
    let base = styled(palette.secondary.base);

    match status {
        Status::Active | Status::Pressed => base,
        Status::Hovered => Style {
            background: Some(Background::Color(palette.secondary.strong.color)),
            ..base
        },
        Status::Disabled => disabled(base),
    }
}

fn mode_selected(theme: &Theme, status: Status) -> Style {
    let palette = theme.extended_palette();
    let base = styled(palette.secondary.base);

    match status {
        Status::Active | Status::Pressed => Style {
            background: Some(Background::Color(palette.secondary.strong.color)),
            ..base
        },
        Status::Hovered => Style {
            background: Some(Background::Color(palette.secondary.strong.color)),
            ..base
        },
        Status::Disabled => disabled(base),
    }
}

