use chrono::NaiveDate;
use std::default::Default;
use std::path::{self};
use std::string::String;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::vec::Vec;

#[derive(Clone, Debug, PartialEq)]
pub enum EventType {
    Regular,
    Background,
}

#[derive(Clone, Debug)]
pub struct Organizer {
	pub email: String,
	pub common_name: Option<String>,
}

#[derive(Clone, Debug)]
pub struct Event {
    pub text: String,
    pub ev_type: EventType,
    pub begin: NaiveDate, // begin of event, inclusive
    pub end: NaiveDate, // end of event, exclusive
    pub categories: Vec<String>,
    pub organizer: Option<Organizer>,
}

impl Event {
    pub fn is_in_day(&self, day: NaiveDate) -> bool {
        day >= self.begin && day < self.end
    }

    pub fn is_in_days(&self, first_day: NaiveDate, last_day: NaiveDate) -> bool {
        let mut day = first_day;
        while day <= last_day {
            if self.is_in_day(day) {
                return true;
            }
            day = day.succ_opt().unwrap();
        }
        false
    }

    pub fn span_days(&self) -> i64 {
        (self.end - self.begin).num_days()
    }
}

pub struct EventsCollection {
    events: Vec<Event>,
}

impl EventsCollection {
    pub fn new() -> Self {
        EventsCollection { events: Vec::new() }
    }

    pub fn clear(&mut self) {
        self.events.clear();
    }

    pub fn create(
        self: &mut Self,
        begin: NaiveDate,
        end: NaiveDate,
        text: &str,
        ev_type: &EventType,
        categories: Vec<String>,
        organizer: Option<Organizer>,
    ) {
        self.events.push(Event {
            text: text.to_string(),
            ev_type: ev_type.clone(),
            begin,
            end,
            categories,
            organizer,
        });
    }

    pub fn is_any_in_day(&self, day: NaiveDate, ev_type: &EventType) -> Option<Event> {
        for ev_day in &self.events {
            if ev_day.is_in_day(day) && &ev_day.ev_type == ev_type {
                return Some(ev_day.clone());
            }
        }
        None
    }

    pub fn for_day(&self, day: NaiveDate) -> Vec<Event> {
        let mut events_in_day = Vec::new();
        for ev_day in &self.events {
            if ev_day.is_in_day(day) {
                events_in_day.push(ev_day.clone());
            }
        }
        events_in_day
    }

    pub fn starting_at(&self, day: NaiveDate) -> Vec<Event> {
        let mut events_in_day = Vec::new();
        for ev_day in &self.events {
            if ev_day.begin == day {
                events_in_day.push(ev_day.clone());
            }
        }
        events_in_day
    }

    pub fn starting_before(&self, day: NaiveDate) -> Vec<Event> {
        let mut events_in_day = Vec::new();
        for ev_day in &self.events {
            if ev_day.begin < day {
                events_in_day.push(ev_day.clone());
            }
        }
        events_in_day
    }

    pub fn within(&self, first_day: NaiveDate, last_day: NaiveDate) -> Vec<Event> {
        let mut events_in_day = Vec::new();
        for ev_day in &self.events {
            if ev_day.is_in_days(first_day, last_day) {
                events_in_day.push(ev_day.clone());
            }
        }
        events_in_day
    }

    pub fn sort(&mut self) {
        self.events.sort_by_key(|event| event.begin);
    }
}

impl Default for EventsCollection {
    fn default() -> EventsCollection {
        EventsCollection::new()
    }
}

pub struct CalendarFile {
    pub file_path: path::PathBuf,
    pub name: String,
    pub enabled: bool,
    pub index: usize,
}

impl CalendarFile {
    pub fn from_file(file_path: path::PathBuf) -> Self {
        static COUNTER: AtomicUsize = AtomicUsize::new(0);
        let name = if let Some(oss_name) = file_path.file_name() {
            if let Some(str) = oss_name.to_str() {
                String::from(str)
            } else {
                String::new()
            }
        } else {
            String::new()
        };

        CalendarFile {
            file_path,
            name,
            enabled: true,
            index: COUNTER.fetch_add(1, Ordering::Relaxed),
        }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }
}
