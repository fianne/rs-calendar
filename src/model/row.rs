use chrono::{Datelike, Duration, Months, Days, NaiveDate, Weekday};
use std::vec::Vec;

#[derive(Debug)]
pub struct CalendarRow {
    pub begin: NaiveDate, // first actual day in the row
    pub end: NaiveDate, // the day just after the last one in the row
}

pub enum RowDay {
    InRange(NaiveDate),
    NotInRange,
}

impl RowDay {
    pub fn new(row: &CalendarRow, day: NaiveDate) -> RowDay {
        if day >= row.begin && day < row.end {
            RowDay::InRange(day)
        } else {
            RowDay::NotInRange
        }
    }
}

/// A row in the calendar.
///
/// The row is always aligned to a week, i.e. the first
/// day of the row is the first day of the week (for now always Monday)
impl CalendarRow {
    /// A row that spans the month containing the given day
    pub fn for_month(day: NaiveDate) -> CalendarRow {
        let begin = day.with_day0(0).unwrap();
        CalendarRow {
            begin,
            end: begin + Months::new(1),
        }
    }

    /// A row that spans the week containing the given day
    pub fn for_week(day: NaiveDate) -> CalendarRow {
        let begin = day.week(Weekday::Mon).first_day();
        CalendarRow {
            begin,
            end: begin + Duration::try_weeks(1).unwrap(),
        }
    }

    /// Get the day for the given column of the row
    pub fn date_for_col(self: &Self, col: i64) -> RowDay {
        RowDay::new(
            self,
            self.begin.week(Weekday::Mon).first_day() + Duration::try_days(col).unwrap(),
        )
    }

    pub fn num_days(&self) -> i64 {
        (self.end - self.begin).num_days()
    }

    pub fn num_cols(&self) -> i64 {
        (self.last_day() - self.first_day()).num_days() + 1
    }

	pub fn first_day(&self) -> NaiveDate {
		self.begin.week(Weekday::Mon).first_day()
	}

	pub fn last_day(&self) -> NaiveDate {
		self.end.pred_opt().unwrap().week(Weekday::Mon).last_day()
	}
}

pub struct CalendarPage {
	col_count: usize,
    rows: Vec<CalendarRow>,
}

impl CalendarPage {
    pub fn new_month_wide(row_count: u32, day: NaiveDate) -> Self {
        let mut page = CalendarPage {
			col_count: 37,
            rows: Vec::with_capacity(row_count as usize),
        };
        for row_num in 0..row_count {
            page.rows.push(CalendarRow::for_month(day + Months::new(row_num)));
        }
        return page;
    }

    pub fn new_week_wide(row_count: u32, day: NaiveDate) -> Self {
        let mut page = CalendarPage {
			col_count: 7,
            rows: Vec::with_capacity(row_count as usize),
        };
        for row_num in 0..row_count {
			let days_offset = Days::new((row_num as u64) * 7);
            page.rows.push(CalendarRow::for_week(day + days_offset));
        }
        return page;
    }

	pub fn get_row_count(&self) -> usize {
		self.rows.len()
	}

	pub fn get_rows(&self) -> &[CalendarRow] {
		&self.rows
	}

	pub fn get_col_count(&self) -> usize {
		self.col_count
	}
}
