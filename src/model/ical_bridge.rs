use icalendar;
use icalendar::{Component, DatePerhapsTime, CalendarDateTime, CalendarComponent};
use chrono::NaiveDate;
use crate::model::events::{EventsCollection, Organizer};
use std::fs;
use log::{warn, debug};

use super::events::EventType;

fn cdt_as_date(cdt: &CalendarDateTime) -> NaiveDate {
    match cdt {
        CalendarDateTime::Floating(ndt) => ndt.date(),
        CalendarDateTime::Utc(ud) => ud.naive_local().date(),
        CalendarDateTime::WithTimezone{date_time, tzid: _} => date_time.date(),
    }
}

fn as_date(cal_date: &DatePerhapsTime) -> NaiveDate {
    match cal_date {
        DatePerhapsTime::DateTime(cdt) => cdt_as_date(cdt),
        DatePerhapsTime::Date(nd) => nd.clone()
    }
}

fn create_event(events: &mut EventsCollection, event: &icalendar::Event, ev_type: &EventType) {
	debug!("  calendar event: {:?}", event);

	let event_property = event.property_value("CATEGORIES").unwrap_or("");
	let event_organizer = event.properties().get("ORGANIZER");
	let organizer = if let Some(property) = event_organizer {
		Some(Organizer{
			email: String::from(property.value()),
			common_name: property.get_param_as("CN", |p| Some(String::from(p))),
			})
	} else {
		Option::None
	};

    events.create(
		as_date(&event.get_start().unwrap()),
		as_date(&event.get_end().unwrap()),
		event.get_summary().unwrap(),
		ev_type,
		event_property.split(',').map(String::from).collect(),
		organizer,
		);
}

pub fn load_calendar(calendar_path: &std::path::Path, events: &mut EventsCollection) {
    let mut calendar = icalendar::Calendar::new();

	debug!("reading calendar: {:?}", calendar_path);

    let contents = match fs::read_to_string(calendar_path) {
        Ok(file_content) => file_content,
        Err(e) => { warn!("Could not read the calendar: {}", e); return; },
    };

	// make sure to handle wrapped lines (as per RFC 5545 section 3.1)
	let contents = icalendar::parser::unfold(&contents);

    let res = icalendar::parser::read_calendar(&contents);
    match res {
        Ok(parsed_calendar) => { calendar.extend(parsed_calendar.components); },
        Err(e) => { warn!("Error parsing calendar {}", e); return; }
    }

	let stem = calendar_path.file_stem().unwrap().to_str().unwrap_or("");

	// TODO: keep it simple for now, just use a filename coding
	let ev_type = if stem.ends_with("_bg") {
		EventType::Background
	}
	else {
		EventType::Regular
	};

    for cc in calendar.iter() {
        match cc {
            CalendarComponent::Event(event) => create_event(events, event, &ev_type),
            _ => (),
        }
    }

    events.sort();
}

