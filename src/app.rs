
use crate::model::{
	events::{EventsCollection, CalendarFile},
	ical_bridge::load_calendar,
};
use crate::ui::{
	controls::Controls,
	calendar::{
		CalendarParams,
		CalendarView,
		CalendarViewMode as ViewMode,
	}
};
#[cfg(feature = "tracing")]
use crate::tracepoints;
use chrono::{NaiveDate, Months, Utc, Days};
use std::path;
use iced::{
    widget::{
		checkbox, column, container, pane_grid::{self, PaneGrid}, Column, Container
	}, Alignment, Element, Length, Task
};

#[derive(Debug, Clone, Copy)]
pub enum Message {
    NextWeek,
    PrevWeek,
    NextMonth,
    PrevMonth,
    NextYear,
    PrevYear,
    ViewModeSelected(ViewMode),
    ToggleSidebar,
    Resized(pane_grid::ResizeEvent),
    UpdateEvents,
    ToggleCalendar(usize),
    ToggleShowOrganizer,
}

#[derive(PartialEq, Eq)]
enum PaneType {
	Calendar,
	Sidebar,
}

struct PaneGridState {
	pane_type: PaneType,
}

pub struct CalendarApp {
    view_date: NaiveDate,
    controls: Controls,
    grid_state: pane_grid::State<PaneGridState>,
    main_pane: pane_grid::Pane,
    split_value: f32,
    events: EventsCollection,
    calendars: std::vec::Vec<CalendarFile>,
    calendar_params: CalendarParams,
}

impl Default for CalendarApp {

    fn default() -> Self {
		let (grid_state, main_pane) = pane_grid::State::new(PaneGridState{pane_type: PaneType::Calendar});

        CalendarApp {
			view_date: NaiveDate::default(),
			controls: Controls::default(),
			grid_state,
			main_pane,
			split_value: 0.20,
			events: EventsCollection::default(),
			calendars: std::vec::Vec::new(),
			calendar_params: CalendarParams::new(),
			}
    }
}

impl CalendarApp {
	pub fn with_files(calendar_paths: std::vec::Vec<path::PathBuf>) -> Self {
        let view_date = Utc::now().date_naive();
        let calendars = calendar_paths.iter()
        	.map(|file_path| {CalendarFile::from_file(file_path.to_path_buf())})
        	.collect();
        let mut app = CalendarApp {
            view_date,
            events: EventsCollection::new(),
            calendars,
            ..CalendarApp::default()
        };
        app.update_events();
        app
	}

	fn update_events(&mut self) {
		self.events.clear();
        for calendar_file in &self.calendars {
			if calendar_file.enabled {
            	load_calendar(&calendar_file.file_path, &mut self.events);
			}
        }
	}

    pub fn update(&mut self, message: Message) -> Task<Message> {
        match message {
            Message::PrevWeek => {
                self.view_date = self.view_date - Days::new(7);
            }
            Message::NextWeek => {
                self.view_date = self.view_date + Days::new(7);
            }
            Message::PrevMonth => {
                self.view_date = self.view_date - Months::new(1);
            }
            Message::NextMonth => {
                self.view_date = self.view_date + Months::new(1);
            }
            Message::PrevYear => {
                self.view_date = self.view_date - Months::new(12);
            }
            Message::NextYear => {
                self.view_date = self.view_date + Months::new(12);
            }
            Message::ViewModeSelected(mode) => {
                self.controls.mode = Some(mode);
            }
            Message::ToggleSidebar => {
				self.toggle_sidebar();
			},
            Message::Resized(pane_grid::ResizeEvent { split, ratio }) => {
				self.split_value = ratio;
                self.grid_state.resize(split, ratio);
            },
            Message::UpdateEvents => {
				self.update_events();
			},
            Message::ToggleCalendar(index) => {
				for calendar_file in &mut self.calendars {
					if calendar_file.index == index {
						calendar_file.enabled = ! calendar_file.enabled;
					}
				}
				return Task::done(Message::UpdateEvents)
			},
            Message::ToggleShowOrganizer => self.calendar_params.ev_show_organizer = ! self.calendar_params.ev_show_organizer,
        }
        Task::none()
    }

	fn toggle_sidebar(&mut self) {
		if let Some(sidebar_pane) = self.get_sidebar_pane() {
			// close the sidebar
			self.grid_state.close(sidebar_pane);
		} else {
			// no sidebar: split the main pane (calendar) and move the new pane to the left
			if let Some((new_pane, _split)) = self.grid_state.split(pane_grid::Axis::Vertical, self.main_pane, PaneGridState{pane_type: PaneType::Sidebar}) {
				self.grid_state.move_to_edge(new_pane, pane_grid::Edge::Left);
				let root = self.grid_state.layout();
				if let pane_grid::Node::Split { id, axis: _, ratio: _, a: _, b: _ } = root {
					self.grid_state.resize(*id, self.split_value);
    			}
			};
		}
	}

	fn get_sidebar_pane(&self) -> Option<pane_grid::Pane> {
		let sb_pane = self.grid_state.iter().find(|&(_, pgs)| {pgs.pane_type == PaneType::Sidebar});
		if let Some((pane, _)) = sb_pane {
			Some(*pane)
		} else {
			None
		}
	}

	fn get_calendar_view(&self) -> CalendarView {
		CalendarView::new(
			self.controls.mode.unwrap_or(ViewMode::Year),
			&self.calendar_params,
			self.view_date,
			&self.events)
	}

	fn get_sidebar_view(&self) -> Container<Message> {
		//container(self.calendars.iter().map(|name| {text(name.as_str())}))
		let mut col = column!("Calendars:")
			.align_x(Alignment::Start);
		for calendar_file in &self.calendars {
			col = col.push(
				checkbox(calendar_file.get_name().as_str(), calendar_file.enabled)
					.on_toggle(|_enable| {Message::ToggleCalendar(calendar_file.index)})
				);
		}
		col = col.push("Options:");
			col = col.push(
				checkbox("show organizer", self.calendar_params.ev_show_organizer)
					.on_toggle(|_enable| {Message::ToggleShowOrganizer})
				);

		container(col)
	}

    pub fn view(&self) -> Element<Message> {
        #[cfg(feature = "tracing")]
        tracepoints::calendar::view_entry();
        let main_content = PaneGrid::new(&self.grid_state, |_id, pane, _is_maximized| {
			match pane.pane_type {
			    PaneType::Calendar => pane_grid::Content::new(self.get_calendar_view()),
			    PaneType::Sidebar => pane_grid::Content::new(self.get_sidebar_view()),
			}
		}).on_resize(10, Message::Resized);
        let content = Column::new()
            .align_x(Alignment::Center)
            .push(self.controls.view(self.view_date))
            .push(main_content);

        let container = Container::new(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .center_x(Length::Fill)
            .center_y(Length::Fill)
            .into();

        #[cfg(feature = "tracing")]
        tracepoints::calendar::view_exit();

        container
    }
}
