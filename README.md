# A calendar viewer in written in rust

`calendar` is focused on displaying one or more files containing events in `ical` format

it is currently in an early development phase.

As a dependency, it requires the iced GUI repository to be cloned alongside (see `Cargo.toml`)

## supported views

* month view
* year view

## input calendars

Clandars are read only from ical files, which are taken:

* from command line if given as arguments
* from `<home>/.local/share/calendars/` if no argument is given

### Public holidays

* calendar files that are named `<name>_bg.ical` are treated as defining public holidays
  and rendered by changing the background of the day cell

## planned features

* reload the events from a calendar file if the file changes
* there is currently no plan to connect to a system calendar (e.g. evolution) or remote.
